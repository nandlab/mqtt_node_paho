#include "mqtt_node_paho.hpp"
#include <boost/bind/bind.hpp>
#include <boost/asio/post.hpp>
#include <boost/asio/placeholders.hpp>
#include <chrono>
#include <sstream>
#include <iomanip>

using namespace boost::placeholders;

using std::string;
typedef mqtt_node_paho self;

const string self::topic_status ("status");
const string self::topic_signal ("signal");
const string self::status_conn              ("ONLINE");
const string self::status_disconn_clean     ("OFFLINE");
const string self::status_disconn_abnorm    ("FAILED");
const string self::status_disconn_exception ("PANIC");
const string self::signal_kill  ("KILL");
const std::chrono::seconds mqtt_node_paho::reconnect_delay (10);

mqtt_node_paho::mqtt_node_paho(
    boost::asio::io_context &ioc,
    std::string node_name,
    std::string broker_uri
)
    : exiting(false),
      ioc(ioc),
      node_name(std::move(node_name)),
      full_topic_signal(full_topic(topic_signal)),
      full_topic_status(full_topic(topic_status)),
      connect_options(
          mqtt::connect_options_builder()
          .automatic_reconnect(false)
          .will(mqtt::message(full_topic_status, status_disconn_abnorm, 0, true))
          .finalize()),
      mqtt_client(broker_uri, node_name),
      reconnect_timer(ioc),
      system_signals(ioc, SIGINT, SIGTERM, SIGHUP)
{
    mqtt_client.set_callback(*this);
}

mqtt_node_paho::~mqtt_node_paho() {
    exiting = true;
    try {
        mqtt_client.disable_callbacks();
        if (mqtt_client.is_connected()) {
            mqtt_client.publish(full_topic_status, status_disconn_exception, 0, true);
            mqtt_client.disconnect()->wait();
        }
    }
    catch (const mqtt::exception &e) {
        std::cout << debug_prefix() << "Exception in mqtt_node_paho dtor: " << e.what() << "." << std::endl;
    }
}

static std::string time_str() {
    using namespace std;
    chrono::system_clock::time_point currentTime = chrono::system_clock::now();
    const time_t time = chrono::system_clock::to_time_t(currentTime);
    chrono::milliseconds ms =
            chrono::duration_cast<std::chrono::milliseconds>(currentTime.time_since_epoch())
            % chrono::milliseconds(1000);
    if (time == (time_t)(-1)) {
        return "NOTIME0";
    }
    else {
        const tm *tm = gmtime(&time);
        if (!tm) {
            return "NOTIME1";
        }
        else {
            ostringstream oss;
            oss << put_time(tm, "%Y-%m-%d %H:%M:%S") << "." << setfill('0') << setw(3) << ms.count();
            return oss.str();
        }
    }
}

/*
std::string mqtt_node_paho::classname() {
    return "mqtt_node_paho";
}
*/

std::string mqtt_node_paho::debug_prefix() {
    return "[" + time_str() + "] " + node_name + ": ";;
}

void mqtt_node_paho::connected(const std::string& cause) {
    boost::asio::post(ioc, boost::bind(&mqtt_node_paho::connected_ioc, this,
                                       std::make_shared<const std::string>(cause)));
}

void mqtt_node_paho::connected_ioc(mqtt::string_ptr cause) {
    if (!exiting) {
        std::cout << debug_prefix() << "Connected";
        if(cause->size()) {
            std::cout << ", cause: " << *cause;
        }
        std::cout << std::endl;
        connected_action_catch();
    }
}

void mqtt_node_paho::connected_action() {
    std::cout << debug_prefix() << "Subscribing to topic '" << full_topic_signal << "'" << std::endl;
    mqtt_client.subscribe(full_topic_signal, 0);
    std::cout << debug_prefix() << "Publishing status '" << status_conn << "' on topic '" << full_topic_status << "'" << std::endl;
    mqtt_client.publish(full_topic_status, status_conn, 0, true);
}

void mqtt_node_paho::connected_action_catch() {
    try {
        connected_action();
    }
    catch (const mqtt::exception &e) {
        std::cout << debug_prefix() << "connected_action() threw " << e.what() << std::endl;
    }
}

void mqtt_node_paho::connection_lost(const std::string& cause) {
    boost::asio::post(ioc, boost::bind(&mqtt_node_paho::connection_lost_ioc, this,
                                       std::make_shared<const std::string>(cause)));
}

void mqtt_node_paho::connection_lost_ioc(mqtt::string_ptr cause) {
    if (!exiting) {
        std::cout << debug_prefix() << "Connection lost!";
        if(cause->size()) {
            std::cout << " Cause: " << *cause;
        }
        std::cout << std::endl;
        start_reconnect_timer();
    }
}

void mqtt_node_paho::message_arrived(mqtt::const_message_ptr msg) {
    boost::asio::post(ioc, boost::bind(&mqtt_node_paho::message_arrived_ioc, this, msg));
}

void mqtt_node_paho::message_arrived_ioc(mqtt::const_message_ptr msg) {
    if (!exiting) {
        std::cout << debug_prefix() << "Message arrived: " << msg->get_topic() << " " << msg->to_string() << std::endl;
        if (!interpret_message(msg)) {
            std::cout << debug_prefix() << "Message unknown" << std::endl;
        }
    }
}

bool mqtt_node_paho::interpret_message(mqtt::const_message_ptr msg) {
    if (msg->get_topic() == full_topic_signal && msg->to_string() == signal_kill) {
        std::cout << debug_prefix() << "Message is kill, exiting ..." << std::endl;
        clean_exit();
        return true;
    }
    return false;
}

void mqtt_node_paho::on_failure(const mqtt::token &token) {
    (void)token;
    boost::asio::post(ioc, boost::bind(&mqtt_node_paho::on_failure_ioc, this));
}

void mqtt_node_paho::on_failure_ioc() {
    if (!exiting) {
        std::cout << debug_prefix() << "Connection attempt failed!" << std::endl;
        start_reconnect_timer();
    }
}

void mqtt_node_paho::on_success(const mqtt::token &token) {
    (void)token;
    boost::asio::post(ioc, boost::bind(&mqtt_node_paho::on_success_ioc, this));
}

void mqtt_node_paho::on_success_ioc() {
    if (!exiting) {
        std::cout << debug_prefix() << "Connection success" << std::endl;
    }
}

void mqtt_node_paho::start_reconnect_timer() {
    std::cout << debug_prefix() << "Reconnecting in " << reconnect_delay.count() << "s ..." << std::endl;
    reconnect_timer.expires_after(reconnect_delay);
    reconnect_timer.async_wait(boost::bind(&mqtt_node_paho::reconnect, this, boost::asio::placeholders::error));
}

void mqtt_node_paho::reconnect(const boost::system::error_code &ec) {
    if (!ec && !exiting && !mqtt_client.is_connected()) {
        std::cout << debug_prefix() << "Reconnecting now ..." << std::endl;
        mqtt_client.connect(connect_options, nullptr, *this);
    }
}

std::string mqtt_node_paho::full_topic(const std::string &topic) {
    return node_name + "/" + topic;
}

void mqtt_node_paho::clean_exit() {
    exiting = true;
    system_signals.cancel();
    reconnect_timer.cancel();
    try {
        std::cout << debug_prefix() << "Publishing status '" << status_disconn_clean
                  << "' on topic '" << full_topic_status << "' ..." << std::endl;
        mqtt_client.publish(full_topic_status, status_disconn_clean, 0, true)->wait();
        std::cout << debug_prefix() << "Offline status published" << std::endl;
    }
    catch (const mqtt::exception &e) {
        std::cout << debug_prefix() << "Publishing offline status threw " << e.what() << std::endl;
    }
    try {
        std::cout << debug_prefix() << "Disconnecting from broker ..." << std::endl;
        mqtt_client.disconnect()->wait();
        std::cout << debug_prefix() << "Disconnected" << std::endl;
    }
    catch (const mqtt::exception &e) {
        std::cout << debug_prefix() << "Disconnect threw " << e.what() << std::endl;
    }
    try {
        mqtt_client.disable_callbacks();
    }
    catch (const mqtt::exception &e) {
        std::cout << debug_prefix() << "disable_callbacks() threw " << e.what() << std::endl;
    }
}

void mqtt_node_paho::system_signal_handler(const boost::system::error_code &ec, int signal) {
    if (!ec && !exiting) {
        std::cout << debug_prefix() << "Signal " << signal << " received" << std::endl;
        clean_exit();
    }
}

void mqtt_node_paho::start() {
    exiting = false;
    std::cout << debug_prefix() << "Connecting to the mqtt broker ..." << std::endl;
    system_signals.async_wait(boost::bind(&mqtt_node_paho::system_signal_handler, this, _1, _2));
    mqtt_client.connect(connect_options, nullptr, *this);
}
