#ifndef MQTTNODEPAHO_HPP
#define MQTTNODEPAHO_HPP

#include <string>
#include <boost/asio/io_context.hpp>
#include <boost/asio/signal_set.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/system/error_code.hpp>
#include "mqtt/async_client.h"

class mqtt_node_paho : public mqtt::callback, public mqtt::iaction_listener
{
protected:
    bool exiting;

    static const std::string topic_signal;

    boost::asio::io_context &ioc;
    const std::string node_name;

    const std::string full_topic_signal;

    /**
     * @brief Prepend "<node_name>/" to the topic
     * @param topic
     * @return full topic path
     */
    std::string full_topic(const std::string &topic);


    std::string debug_prefix();

    /**
     * @brief connected_action
     * Procedure to be called, when the the connection
     * to the MQTT broker is established.
     */
    virtual void connected_action();

    /**
     * @brief connected_action_catch
     * Wraps connected_action() into a try block and catches thrown exceptions.
     */
    virtual void connected_action_catch();

    /**
     * @brief interpret_arrived_message
     * @param msg the arrived mqtt message
     * @return true if the message was interpreted successfully, false if the message is unknown
     */
    virtual bool interpret_message(mqtt::const_message_ptr msg);

    virtual void clean_exit();

    // virtual std::string classname();

private:
    static const std::string topic_status;
    static const std::string status_conn;
    static const std::string status_disconn_clean;
    static const std::string status_disconn_abnorm;
    static const std::string status_disconn_exception;
    static const std::string signal_kill;
    static const std::chrono::seconds reconnect_delay;

    const std::string full_topic_status;
    mqtt::connect_options connect_options;

protected:
    mqtt::async_client mqtt_client;
private:
    boost::asio::steady_timer reconnect_timer;
    boost::asio::signal_set system_signals;

    void system_signal_handler(const boost::system::error_code &ec, int signal);

    // Main client callbacks
    void connected(const std::string& cause) override;
    void connected_ioc(mqtt::string_ptr cause);

    void connection_lost(const std::string& cause) override;
    void connection_lost_ioc(mqtt::string_ptr cause);

    void message_arrived(mqtt::const_message_ptr msg) override;
    void message_arrived_ioc(mqtt::const_message_ptr msg);

    // void delivery_complete(mqtt::delivery_token_ptr tok) override;

    // Initial connect callbacks
    void on_failure(const mqtt::token &token) override;
    void on_failure_ioc();

    void on_success(const mqtt::token &token) override;
    void on_success_ioc();

    void start_reconnect_timer();

    void reconnect(const boost::system::error_code &ec);

public:
    mqtt_node_paho(boost::asio::io_context &ioc,
                   std::string node_name,
                   std::string broker_uri);
    virtual ~mqtt_node_paho() override;
    virtual void start();
};

#endif // MQTTNODEPAHO_HPP
